import { useParams } from 'react-router-dom';
import { Navigate } from "react-router-dom";
import lodgingData from '../data/logements.json';
import Carousel from '../components/Carousel';
import Location from '../components/Location';
import Host from '../components/Host';
import Card_info from '../components/Card_Info';
import '../styles/Lodging.css';

function Lodging() {

  const { id } = useParams();
  const lodgingFind = lodgingData.find(element => element.id === id);
  if(lodgingFind === undefined){
    return(<Navigate to="/*" />);
  }else {
    return (
      <div className="lodging_div">
        <Carousel pictures={lodgingFind.pictures} />
        <Location title={lodgingFind.title} location={lodgingFind.location} tags={lodgingFind.tags}/>
        <Host host={lodgingFind.host} rating={lodgingFind.rating}/>
        <div className="div_card_info_lodging">
          <Card_info title="Description" contained={lodgingFind.description} type="text" page="lodging"/>
        </div>
        <div className="div_card_info_lodging">
          <Card_info title="Équipements" contained={lodgingFind.equipments} type="list" page="lodging"/>
        </div>
      </div>
    );
  }
}

export default Lodging;
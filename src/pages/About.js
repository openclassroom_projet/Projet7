import aboutData from '../data/about.json';
import Card_info from '../components/Card_Info';
import imgAbout from '../assets/About_img.png';
import '../styles/About.css';

function About() {
  return (
    <div className="about_div">
      <div className="div_banner_about">
        <img src={imgAbout} alt="Bannière de la page A propos" className="about_picture"/>
      </div>
      {
        aboutData.map((aboutFind) =>
        <div key={aboutFind.id} className="div_card_info_about">
          {
            <Card_info title={aboutFind.title} contained={aboutFind.text} type="text" page="about"/>
          }
        </div>
        )
      }
    </div>
  );
}

export default About;

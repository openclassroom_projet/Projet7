import imgHome from '../assets/home_img.png';
import '../styles/Home.css';
import listLodging from '../data/logements.json';
import { Link } from 'react-router-dom';

function Home() {
  return (
    <div className="home_div">
      <div className="div_banner_home">
        <h1 className="title_banner_home">Chez vous, partout et ailleurs</h1>
        <img src={imgHome} alt="Bannière de la page d'accueil" className="home_picture"/>
      </div>
      <div className="div_list_card">
        {
          listLodging.map((logement) => 
            <div key={logement.id} className="div_card">
              <Link to={"/lodging/"+logement.id} className="link_card">
                <p className="title_card">{logement.title}</p>
                <img src={logement.cover} alt={"Couverture de " + logement.title} className="img_card"/>
              </Link>
            </div>
          )
        }
      </div>
    </div>
  );
}

export default Home;

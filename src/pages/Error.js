import '../styles/Error.css';
import { Link } from 'react-router-dom';

function Error() {
    return (
      <div className="div_error">
        <p className="text_error">404</p>
        <p className="description_error">Oups! La page que vous demandez n'existe pas.</p>
        <Link to="/" className="link_return">
          Retourner sur la page d’accueil
        </Link>
      </div>
    );
  }
  
  export default Error;
import good from '../assets/Star_good.svg';
import notGood from '../assets/Star_not_good.svg';
import '../styles/Rating.css'

function Rating(props) {
    const star = [1, 2, 3, 4, 5];

    return(
        <div className="div_rating">
            {
                star.map((elementStar, index) => 
                    elementStar <= props.nbRating ? <img 
                        key={index} 
                        src={good} 
                        alt="rating good" 
                        className="rating_img"
                    /> : <img 
                        key={index} 
                        src={notGood} 
                        alt="rating good" 
                        className="rating_img"
                    />
                )
            }
        </div>
    );
}

export default Rating;
import '../styles/Location.css';

function Location(props) {
    return(
        <div className="div_location">
            <p className="location_title">{props.title}</p>
            <p className="location">{props.location}</p>
            <div className="div_list_tags">
                {
                    props.tags.map((tag, index) => 
                        <p key={index} className="tag">{tag}</p>
                    )
                }
            </div>
        </div>
    );
}

export default Location;
import '../styles/Footer.css';
import logo from '../assets/LOGO_footer.svg';

function Footer() {
    return(
        <div className="div_footer">
            <img src={logo} alt="Logo du footer" className="img_footer"/>
            <p className="text_footer">© 2020 Kasa. All rights reserved</p>
        </div>
    );
}

export default Footer;
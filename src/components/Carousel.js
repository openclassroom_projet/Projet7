import { useState } from 'react';
import '../styles/Carousel.css';
import back from '../assets/arrow_back.png';
import forward from '../assets/arrow_forward.png';

function Carousel(props) {
    const [count, setCount] = useState(0);
    const propsLength = Object.keys(props.pictures).length;
    return(
        <div className="div_carousel">
            <img src={props.pictures[count]} alt={"Image numero "+count} className="img_carousel"/>
            <div className="div_carousel_arrow">
                <img 
                    src={back} 
                    alt="Flèche retour du carousel" 
                    className="back"
                    onClick={() => count-1 < 0 ? setCount(propsLength-1) : setCount(count-1)}
                />
                <img src={forward} 
                    alt="Flèche suivant du carousel" 
                    className="forward"
                    onClick={() => count+1 === propsLength ? setCount(0) : setCount(count+1)}
                />
            </div>
        </div>
    );
}

export default Carousel;
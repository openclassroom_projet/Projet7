import '../styles/Card_contained.css';

function Card_contained(props) {
    return(
        <div className={
            props.show === false ? "none_card" : "card"
        }>
            {
                props.type === "text" ? 
                <p className={ props.page === "lodging" ? "text_card_lodging" : "text_card_about" }>
                    {props.contained}
                </p> : 
                props.contained.map((element, index) =>
                    <p 
                        key={index} 
                        className={ 
                            props.page === "lodging" ? "text_card_lodging" : "text_card_about"
                        }
                    >
                        {element}
                    </p>
                )
            }
        </div>
    );
}

export default Card_contained;
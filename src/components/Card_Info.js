import { useState, React, Fragment } from 'react';
import Card_contained from '../components/Card_contained'
import '../styles/Card_info.css';
import up from '../assets/arrow_up.png';
import down from '../assets/arrow_down.png';

function Card_info(props){
    const [state, setState] = useState(false);

    return(
        <Fragment>
            <div className="div_card_info">
                <p 
                    className={ props.page === "lodging" ? "title_card_info_lodging" : "title_card_info_about" }
                >
                    {props.title}
                </p>
                <img 
                    src={ 
                        state === false ? down : up
                    }
                    alt="Flèche de déroulement"
                    onClick={() =>
                        state === false ? setState(true) : setState(false)
                    }
                    className="arrow"
                />
            </div>
            <Card_contained contained={props.contained} type={props.type} page={props.page} show={state}/>
        </Fragment>
    );
}

export default Card_info;
import { useState } from 'react';
import Rating from '../components/Rating'
import '../styles/Host.css';

function Host(props) {
    const [count, setCount] = useState(0);
    return(
        <div className="div_host">
            <p className="host_name">{props.host.name}</p>
            <div className="div_img_host">
                <img src={props.host.picture} alt="Photographie de l'hôte" className="img_host"/>
            </div>
            <Rating nbRating={props.rating} />
        </div>
    );
}

export default Host;
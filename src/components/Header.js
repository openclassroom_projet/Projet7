import { NavLink } from "react-router-dom";
import '../styles/Header.css';
import logo from '../assets/LOGO.svg';

function Header() {
  let activeStyle = { textDecoration: "underline" };
  return (
    <div className="header-card">
      <NavLink to="/" className="logo">
        <img src={logo} alt="logo du site" className="logo-img"/>
      </NavLink>
      <div className="nav-card">
        <NavLink 
          to="/" 
          className="nav-link" 
          style={({ isActive }) =>
            isActive ? activeStyle : undefined
          }
        >
          Accueil
        </NavLink>
        <NavLink 
          to="/about" 
          className="nav-link"
          style={({ isActive }) =>
            isActive ? activeStyle : undefined
          }
        >
          A Propos
        </NavLink>
      </div>
    </div>
  );
}

export default Header;